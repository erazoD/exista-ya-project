<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wp_test');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '/qK=+ H{ix(2^9KS6@D-h_g:[9-eDGN<*qh]WyAs?xBjoK1P{ObH/*6Mx=qg!e#-');
define('SECURE_AUTH_KEY', '~n/0d2LBv#B$qRO,GuQ4n;ZFg7q2NbFoo~Pf(c^|J=dnqNMk_^r+Uhh_&b,n-d(r');
define('LOGGED_IN_KEY', '#hjg6lbs`f+I]8A]DqFVFkb@;2_khFFhmGoO^F&+9.c[?p?/} :C&J9$_fi)TI*r');
define('NONCE_KEY', 'q]Sl:e;6*G@H{!rX$jV]%MNWq M[N?5l%ih&@zeAMW%M$|WU3q@lDh4+C-Tv~q84');
define('AUTH_SALT', '2Jx#>^u5f*zDe!kvX$x obHqkUTuuRG@$!vv}VTBHHv]m:=u#7^Gm0_(DTK[.xxP');
define('SECURE_AUTH_SALT', '_r( %w`kq4leaSd5&5!HKcjV`Z:=1R]B3D[jGu$+7{/3A wA!4C9[!b~V_}MrbR]');
define('LOGGED_IN_SALT', 'JLsrNY~(#|muh+f$bwRb%iR)qZbFy#6GXIzg%4<R8PzYoJsK^E<EH}7Mmcldv=1B');
define('NONCE_SALT', 'AM!fmXYV)JiTOK@Ta$}pD:2G;2Widv 1e2%TxB@2{;&*AECsf<q]1>+hwfAxo8[!');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

