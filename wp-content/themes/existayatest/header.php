<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package existayatest
*/

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="profile" href="https://gmpg.org/xfn/11">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

<header id="masthead" class="site-header">
<div class="header-user">
<div class="container">
	<div class="d-flex justify-content-between bd-highlight mb-2">
		<div class="general-info">
			<p class="text-white-50">Contacto:(57)2 884 11 14</p>
			<div class="social">
				<a href=""> <i class="fab fa-facebook-square text-white-50"></i></a>
				<a href="">
					<i class="fab fa-twitter text-white-50"></i> </a>
					<a href="">
						<i class="fab fa-google-plus-g text-white-50"></i> </a>
						<a href="">
							<i class="fab fa-instagram text-white-50"></i> </a></div>
						</div>
						<div class="account-info">
							<a href="#" class='text-white-50'>Centro de Ayuda</a>
							<a href="#" class="text-white-50"><i class="far fa-user"></i> Mi Cuenta</a>	
						</div>
					</div>

				</div>
			</div> <!--header user-->
        
			<div class="main-header">
				<div class="container">
					<div class="d-flex justify-content-between mb-3 adjust-menu-mob">

						<div class="site-branding">
							<a href="<?php echo get_home_url(); ?>"><img class='' src="<?php bloginfo('template_url') ;?>/images/logo-libreria-white.png" alt="">	</a>
						</div><!-- .site-branding -->
                    
        <div class="main-nav"> 
            <nav id="site-navigation" class="nav main-navigation navbar navbar-expand-md">
            	<button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            	<span class="text-white"><i class="fas fa-bars"></i></span>
            	</button>	
			<?php
			wp_nav_menu( array(
				'theme_location' => 'primary',
				'menu_id'        => 'navbarSupportedContent',
				'menu_class'=>'menu-site collapse navbar-collapse'
			) );
			?>
		</nav><!-- #site-navigation -->

        </div>

     <div class="options-nav d-flex justify-content-between">
        	<div class="grey-square">
        	<i class="fas fa-search text-white"></i></div>
        	<a href=""><div class="grey-square"><i class="fas fa-heart text-white"></i></div></a>
        	<div class="grey-square"><i class="fas fa-shopping-cart text-white"></i><div class="black-counter"><span class="text-white">0</span></div></div>
        </div>

					</div>
				</div>
			</div><!-- main - header -->
	<div class="breadcrumb d-flex justify-content-start bg-light-grey">
	<div class="container">
		<?php the_breadcrumb(); ?>
	</div>
	</div>		
	</header><!-- #masthead -->
	 
	<div id="content" class="site-content">
