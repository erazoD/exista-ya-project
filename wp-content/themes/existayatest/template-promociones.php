 <?php
/**
 * Template Name: Promociones
 *
 * @package WordPress
 * @subpackage existaya_theme
 */

get_header(); ?>

<div id="secondary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="container">
			<div class="banners-promo">
				<?php 
         //Recuperar banners y desplegarlos
				$banner_counter = 1;
				$params = array(
        'limit'   => -1  // Return all rows
    );
				$promos = pods( 'promocion', $params ); 
				if ( 0 < $promos->total() ) {
					while ( $promos->fetch() ) {
						?>
						<div><img class='img-fluid' src="<?php echo $promos->field('banner_img._src');?>" alt=""> <p style='text-align:center;'><?php echo $banner_counter++;?> </p></div>  
					<?php }
				}?>	
			</div> <!--BANNERS PROMO-->
			<!--Noticias Promo-->
			<?php get_template_part( 'template-parts/content', 'promos' );  ?>

			<!--Libro de la semana-->
		</div>
	    <?php get_template_part( 'template-parts/content', 'weekly' );  ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();