<?php
/**
 * Template part for displaying page content in template-promociones.php
 *
 * @package existayatest
 */

?>


<div class="bg-light">
	<div class="container">
		<div class="weekly-book-more d-flex justify-content-between border-bottom">
			<h5>Libro de la Semana</h5>
			<a href="#" class='text-dark'>Conocer más <i class="fas fa-angle-right"></i></a>
		</div>
		<div class="weekly-book">
			<?php 
			$score = array();
			$params = array(
                  'limit'   => -1  // Return all rows
              );
			$books = pods( 'libro', $params,  get_the_id() ); 
			if ( 0 < $books->total() ) {
				while ( $books->fetch() ) {

					$related = $books->field( 'resena' );
					foreach($related as $rel){
                          $id = $rel['ID'];
                          $puntaje = get_post_meta( $id, 'calificacion', true );
                          array_push($score, $puntaje);
					}
					 //Calificacion promedio de acuerdo las reseñas generales
					 $score = array_filter($score);
                     $average = round(array_sum($score)/count($score));
					?>
					<div class="row">        
						<div class="book-img col-12  col-sm-12 col-lg-4">
							<img src="<?php echo $books->field('libro_cover._src'); ?>" alt="">
						</div>
						<div class="book-info col-12 col-sm-12 col-lg-8">
							<h2><?php echo $books->display('title'); ?></h2>
							<span> <b>---<?php echo $books->display('autor'); ?></b></span>
							<span>
                                <?php 
                                //Contador de reseñas
                                for ($z = 0; $z < $average; $z++){
                                echo '<i class="fas fa-star"></i>';

                                }
                                if ($average < 5){
                                	$diff = 5 - $average;
                                	 for ($a = 0; $a < $diff; $a++){
                                echo '<i class="far fa-star"></i>';

                                }
                                }
                                ?>
								 <?php echo sizeof($related); ?> Reseñas </span>
							<h3><?php echo $books->display('precio'); ?></h3>
							<p><?php echo $books->display('sinopsis'); ?></p>
						</div>
					</div>
				<?php
			}
			} ?>
		</div>
	</div>
</div>