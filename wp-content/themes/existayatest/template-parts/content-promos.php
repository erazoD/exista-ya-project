<?php
/**
 * Template part for displaying page content in template-promociones.php
 *
 * @package existayatest
 */

?>

<div class="noticias-promo">
			<?php 
              
			$params = array(
		    'orderby' => 'date ASC',
            'limit'   => 3
            );
			$news = pods( 'noticia_promocion', $params );
			
			$publication = 1;

			if ( 0 < $news->total() ) {
				while ( $news->fetch() ) {
                       $publication++;
			?>	
                
            <div class="noticia-promo ">
            	<div class="row d-flex <?php  echo ($publication % 2 == 0)? 'flex-row-reverse' : '';  ?>">
            	 <div class="promo-info col-sm">
            		<span class=''><?php echo $news->field('sub_titulo'); ?></span>
            		<h2><?php echo $news->field('titulo'); ?></h2>
            		<span class=''><i class="fas fa-tag"></i> <?php echo $news->field('condicional'); ?></span>
            		<span class=''><i class="far fa-calendar-alt">  </i>
                        
                         <?php echo $news->field('fecha_inicio'); ?> - <?php echo $news->field('fecha_fin'); ?></span>
            		<a href="<?php echo $news->field('permalink')?>"><button type="button" class="btn btn-secondary"> Leer más información</button></a>
            	</div>

            	<div class="promo-img col-sm">
            		<img class='img-fluid' src="<?php echo $news->field('promo_img._src');?>" alt="">
            	</div>
            	</div>
            </div><!--NOTICIA PROMO-->
				<?php }
			}?>
		</div><!--Noticias PROMO-->