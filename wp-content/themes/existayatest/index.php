<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package existayatest
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<div class="container">
			<div class="banners-promo">
				<?php 
         //Recuperar banners y desplegarlos
				$banner_counter = 1;
				$params = array(
        'limit'   => -1  // Return all rows
    );
				$promos = pods( 'promocion', $params ); 
				if ( 0 < $promos->total() ) {
					while ( $promos->fetch() ) {
						?>
						<div><img class='img-fluid' src="<?php echo $promos->field('banner_img._src');?>" alt=""> <p style='text-align:center;'><?php echo $banner_counter++;?> </p></div>  
					<?php }
				}?>	
			</div> <!--BANNERS PROMO-->
			<!--Noticias Promo-->
			<?php get_template_part( 'template-parts/content', 'promos' );  ?>

			<!--Libro de la semana-->
		</div>
	    <?php get_template_part( 'template-parts/content', 'weekly' );  ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
 
get_footer();
