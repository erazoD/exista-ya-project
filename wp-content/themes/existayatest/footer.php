<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package existayatest
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer">
	<div class=" bg-dark">
	<div class="site-info container">
		<div class="d-flex justify-content-between info-main row">
			<div class="nacional-info  col-sm-12 col-md-6 col-lg-3 col-xl-3">
				<img class='img-fluid' src="<?php bloginfo('template_url') ;?>/images/logo-libreria-white.png" alt="">
				<span class='text-white'> <b>LIBRERIA NACIONAL S.A.</b></span>
				<span class='text-white'>NIT: 890.301.951-0</span>
				<span class='text-white'>Direción: Cra. 5 N° 11-50. Cali - Valle del Cauca</span>
				<span class='text-white'>Email:servicioalcliente@librerianacional.com </span>
				<span class='text-white'>Telefono: (57} 2 884 11 14 </span>
				<span class='text-white'><b>Confederacion Colombiana de Consumidores: </b></span>
				<span class='text-white'>www.ccconsumidores.org.co </span>

				
			</div>
			<div class="site-map col-sm-12 col-md-6 col-lg-3 col-xl-3">
				<p class="text-white"><b>Corporativo</b></p>
				<ul>
					<li><a href="#" class="text-white">Quienes somos</a></li>
					<li><a href="#" class="text-white">Sedes</a></li>
					<li><a href="#" class="text-white">Cafe</a></li>
					<li><a href="#" class="text-white">Regala un bono</a></li>
					<li><a href="#" class="text-white">Eventos</a></li>
				</ul>
			</div>
			<div class="site-map col-sm-12 col-md-6 col-lg-3 col-xl-3">
				<p class="text-white"><b>Centro de ayuda</b></p>
				<ul>
					<li><a href="#" class="text-white">PQRS</a></li>
					<li><a href="#" class="text-white">Preguntas frecuentes</a></li>
					<li><a href="#" class="text-white">Politica de tratamiento de datos personales</a></li>
					<li><a href="#" class="text-white">Politica para cambio de mercancia</a></li>
					<li><a href="#" class="text-white">Manuales</a></li>
				</ul>
			</div> <!-- site-map -->
			<div class="nacional-info col-sm-12  col-md-6 col-lg-3 col-xl-3">
				<div class="pagos">
					<span class="text-white">Aceptamos todos los medios de pago</span>
					<img src="<?php bloginfo('template_url') ;?>/images/tarjetas.png" alt="">
					<span class="text-white">Pagos seguros con: <img src="<?php bloginfo('template_url') ;?>/images/payu-logo.png" alt=""> </span>
				</div>
				<div class="social">
					<a href=""> <i class="fab fa-facebook-square text-white-50"></i></a>
					<a href="">
						<i class="fab fa-twitter text-white-50"></i> </a>
						<a href="">
							<i class="fab fa-google-plus-g text-white-50"></i> </a>
							<a href="">
								<i class="fab fa-instagram text-white-50"></i> </a></div>
							</div> <!-- nacional-info -->
						</div>
						</div>
					<div class="bg-secondary disclaimer-final d-flex justify-content-between">
						<p class="text-white">

Copyright c 2015. Todos los derechos reservados </p>
		<p class="text-white">
   Powered by <a a="" href="http://existaya.com/" target="_blank" class="text text--grisMed"><span class="icon-existayalogo"></span></a> </p>
					</div>	
					</footer><!-- #colophon -->
				</div><!-- #page -->

				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
				<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
				<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

				<?php wp_footer(); ?>

			</body>
			</html>
