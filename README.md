# exista-ya-project

Proyecto de desarrollo front-end para prueba t�cnica de ExistaYa


### How do I get set up? ###
* Clonar el proyecto en servidor local.  
* Importar la base de datos "wp_test" ubicada al inicio del proyecto en una carpeta llamada DB  
* Seleccionar el tema "existayatest" 
* En el home se puede ver el wireframe pero se encuentra m�s completo en la direcci�n "http://localhost/exista-ya-project/promociones/"